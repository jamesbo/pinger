# -*- coding: utf-8 -*- 
 
import pyping, os, sys, dns.resolver
reload(sys)
import locale
sys.setdefaultencoding(locale.getpreferredencoding())

try:
    checkCount = len(list(open(os.path.dirname(sys.argv[0]) + '/route.conf')))
    checkCount = checkCount + len(list(open(os.path.dirname(sys.argv[0]) + '/dns.conf')))
except:
    print(u'Нет файлов конфигурации. Вам сюда:')
    print('https://bitbucket.org/jamesbo/pinger') 
    raw_input('')
else:
    handle = open(os.path.dirname(sys.argv[0]) + '/route.conf', 'r')

    i = 0

    for line in handle:
        i = i + 1
        print('[' + str(i) + '/' + str(checkCount) + u'] Пингую ' + line.strip())

        try:
            response = pyping.ping(line.strip())
            if (response.ret_code == 0):
                print(u'успешно')
            else:
                print(u'неудачно')

        except:
            print(u'Не могу запустить проверку. Вы точно запустили меня от имени администратора?')

    handle.close()


    handle = open(os.path.dirname(sys.argv[0]) + '/dns.conf', 'r')

    resolver = dns.resolver.Resolver(configure=False)

    for line in handle:
        i = i + 1
        print('[' + str(i) + '/' + str(checkCount) + u'] Проверяю днс ' + line.strip())
        resolver.nameservers = [line.strip()]
        try:
            answer = resolver.query('amazon.com', 'A')
            print(u'успешно')
        except:
            print(u'неудачно')


    handle.close()

    print ('')
    print(u'Нажмите ENTER, чтобы закрыть окно')
    raw_input('')